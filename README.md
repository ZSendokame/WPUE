# WPUE
WPUE (WordPress User-Enumeration) it's a easy to use Tool to do User Enumeration on WordPress.<br>
It lets you enumerate all public users in ⚡1 second⚡!.

# How-to Install
<code>git clone https://github.com/zsendokame/WPUE; pip install requests</code><br>

And it's ready to use!

# How-to Use
<code>python wpue.py https://wordpress.site</code>
